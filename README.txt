INTRODUCTION
------------
The Custinv module is an exercise to test the feasibility of a relational data
model using Drupal entities and entity reference fields.

REQUIREMENTS
------------

- Create two Drupal entities: Customer and Invoice.

- Create a relational model that maps customers to their invoices.

- The entities and relationships must be created programmatically. The
model must be able to scale to an infinite number of customers and invoices.

- Once an invoice is created for a customer it cannot be assigned to any
other customer. The relationship is permanent unless the invoice is deleted.


INSTALLATION
------------

CONFIGURATION
-------------
